//1. STATUS: OK

//2. STATUS: OK

		db.fruits.aggregate([
		        {
		            $match: { "onSale": true}
		        },
		        {
		            $count: "fruitsOnSale"
		       	}
		    ])

//3. STATUS: OK

		db.fruits.aggregate([
		        {
		            $match: { "stock": {$gte: 20}}
		        },
		        {
		            $count: "enoughStock"
		       	}
		    ])

//4. STATUS: OK
		db.fruits.aggregate([
		        {
		            $match: { "onSale": true}
		        },
		        //uses id field instead of supplier {result: ave price of stock name}
		        {
		            $group: { _id: "$name", avg_price_per_name: { $avg: "$price" } }
		       	}
		    ])

		db.fruits.aggregate([
		        {
		            $match: { "onSale": true}
		        },
		        //if _id: null, the result would be the ave price of all fruits onSale
		        {
		            $group: { _id: null, avg_price_of_all_fruits: { $avg: "$price" } }
		       	}
		    ])



//5. STATUS: OK

		db.fruits.aggregate([
	        {
	            $match: { "onSale": true}
	        },
	        {
	            $group: { _id: null, max_price: { $max: "$price" } }
	       	}
	    ])


//6. STATUS: OK
	    	db.fruits.aggregate([
	            {
	                $match: { "onSale": true}
	            },
	            {
	                $group: { _id: null, min_price: { $min: "$price" } }
	           	}
	        ])
